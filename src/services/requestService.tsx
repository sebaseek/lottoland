import axios, { AxiosRequestConfig } from 'axios';

export interface IRequestService {
    get(url: string, options?: AxiosRequestConfig): Promise<any>;
}
const RequestService: () => IRequestService = () => {
    const get: (url: string, options?: AxiosRequestConfig) => any = (
        url,
        options
    ) => {
        // Proxy url to perform Request => Cors Error.
        const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';
        return axios.get(`${PROXY_URL}${url}`, {
            method: 'get',
            url: `${PROXY_URL}${url}`
        });
    };

    return { get };
};

export default RequestService;
