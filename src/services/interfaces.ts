export interface IHttpData {
    last: IDataStructure | undefined;
    next: IDataStructure | undefined;
}

export interface IOdds {
    rank0: IRank;
    rank1: IRank;
    rank2: IRank;
    rank3: IRank;
    rank4: IRank;
    rank5: IRank;
    rank6: IRank;
    rank7: IRank;
    rank8: IRank;
    rank9: IRank;
    rank10: IRank;
    rank11: IRank;
    rank12: IRank;
}

export interface IRank {
    winners: number;
    specialPrize: number;
    prize: number;
}
export interface IDate {
    day: number;
    dayOfWeek: string;
    full: string;
    hour: number;
    minute: number;
    month: number;
    year: number;
}
export interface IDataStructure {
    Winners?: number;
    climbedSince: number;
    closingData: string;
    currency: string;
    date: IDate;
    drawingDate: string;
    euroNumbers: number[];
    jackpot: string;
    lateClosingDate: string;
    marketingJackpot: string;
    nr: number;
    numbers: number[];
    odds?: IOdds;
    specialMarketingJackpot: string;
}
