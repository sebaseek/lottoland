// React
import React, { useEffect, useState } from 'react';

// Custom Styles
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

// Components
import HeaderComponent from './components/header/headerComponent';
import JackpotResult from './components/jackpotResult/jackpotResult';
import JackpotTable from './components/jackpotTable/jackpotTable';
import RequestService, { IRequestService } from './services/requestService';

const App: React.FC = () => {
    const urlData = 'https://media.lottoland.com/api/drawings/euroJackpot';
    const [loaded, setLoaded] = useState(false);
    const [last, setLast] = useState(undefined);
    const [next, setNext] = useState(undefined);
    const { get }: IRequestService = RequestService();

    // Fetch data when component loads.
    useEffect(() => {
        get(urlData).then(response => {
            setLast(response.data.last);
            setNext(response.data.next);
            setLoaded(true);
        });
    }, [setLast, setNext]);

    return (
        <>
            <div className="container jackpot-container">
                {loaded ? (
                    <div>
                        <HeaderComponent last={last} next={next} />
                        <hr />
                        <JackpotResult last={last} next={next} />
                        <JackpotTable last={last} next={next} />
                    </div>
                ) : (
                    ''
                )}
            </div>
        </>
    );
};

export default App;
