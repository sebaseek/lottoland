import './jackpotTable.css';
import React from 'react';
import { IHttpData, IOdds, IRank } from '../../services/interfaces';
import {
    Currency,
    CurrencyFormatter,
    CurrencyLocale,
    ICurrencyFormatter
} from '../utils/currencyFormatter';
import Utils, { IUtils } from '../utils/utils';

const JackpotTable: React.FC<IHttpData> = ({ last }) => {
    const { formatCurrency }: ICurrencyFormatter = CurrencyFormatter();
    const { formatNumberToRoman, getMatchTierText }: IUtils = Utils();
    const getTableRowData = (odds: IOdds) => {
        // Convert Object to Array
        // https://stackoverflow.com/questions/25217706/converting-object-to-array-using-es6-features
        let arr: IRank[] = [];
        Object.values(odds).map((odd: IRank) => {
            return arr.push(odd);
        });
        return arr;
    };

    return (
        <>
            {last ? (
                <div>
                    <table className="table">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">Tier</th>
                                <th scope="col">Match</th>
                                <th scope="col">Winners</th>
                                <th scope="col">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            {last && last.odds ? (
                                getTableRowData(last.odds).map(
                                    (odd: IRank, i: number) => {
                                        const { prize, winners } = odd;
                                        if (prize > 0) {
                                            return (
                                                <tr key={i}>
                                                    <td>
                                                        {formatNumberToRoman(i)}
                                                    </td>
                                                    <td>
                                                        {getMatchTierText(i)}
                                                    </td>
                                                    <td>{winners}x</td>
                                                    <td>
                                                        {formatCurrency(
                                                            CurrencyLocale.EN_ES,
                                                            Currency.EUR
                                                        ).format(prize)}
                                                    </td>
                                                </tr>
                                            );
                                        }
                                    }
                                )
                            ) : (
                                <tr>
                                    <td> No results found.</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            ) : (
                ''
            )}
        </>
    );
};

export default JackpotTable;
