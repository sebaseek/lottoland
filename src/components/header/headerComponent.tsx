import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import './headerComponent.css';
import { IDate, IHttpData } from '../../services/interfaces';
import 'react-datepicker/dist/react-datepicker.css';
// CSS Modules, react-datepicker-cssmodules.css
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

const HeaderComponent: React.FC<IHttpData> = ({ last }) => {
    const [startDate, setStartDate] = useState<Date | null>(null);

    const getDate = (date: IDate) => {
        let dateString = '' + date.year + '-' + date.month + '-' + date.day;
        return new Date(dateString);
    };
    return (
        <>
            <header className="header-container">
                <h1 className="header-title">
                    EUROJACKPOT RESULTS & WINNING NUMBERS
                </h1>
                {last ? (
                    <div className="date-picker-container">
                        <DatePicker
                            selected={startDate}
                            onChange={(date: Date) => setStartDate(date)}
                            includeDates={[getDate(last.date)]}
                            placeholderText={'Date'}
                        />
                    </div>
                ) : (
                    ''
                )}
            </header>
        </>
    );
};

export default HeaderComponent;
