import React from 'react';
import './jackpotResult.css';
import { IHttpData } from '../../services/interfaces';

const JackpotResult: React.FC<IHttpData> = ({ last, next }) => {
    return (
        <>
            {last && (
                <div>
                    <span className="jackpot-color">EuroJackpot </span>
                    <span>
                        Results for Friday {last.date.day} {last.date.month}{' '}
                        {last.date.year}{' '}
                    </span>
                    <div className="result-numbers">
                        <ul className="result-numbers-list">
                            {last.numbers.map(
                                (number: number, index: number) => (
                                    <li key={index}>{number}</li>
                                )
                            )}
                            {last.euroNumbers.map(
                                (number: number, index: number) => (
                                    <li className="jackpot-color" key={index}>
                                        {number}
                                    </li>
                                )
                            )}
                        </ul>
                    </div>
                </div>
            )}
        </>
    );
};

export default JackpotResult;
