export interface IUtils {
    formatNumberToRoman(n: number): string;
    getMatchTierText(i: number): string;
}

export interface ITierList {
    tierId: number;
    matchText: string;
}

const Utils: () => IUtils = () => {
    // Tier list definition
    const tierList: ITierList[] = [
        { tierId: 1, matchText: '5 Numbers + 2 Euronumbers' },
        { tierId: 2, matchText: '5 Numbers + 1 Euronumbers' },
        { tierId: 3, matchText: '5 Numbers + 0 Euronumbers' },
        { tierId: 4, matchText: '4 Numbers + 2 Euronumbers' },
        { tierId: 5, matchText: '4 Numbers + 1 Euronumbers' },
        { tierId: 6, matchText: '4 Numbers + 0 Euronumbers' },
        { tierId: 7, matchText: '3 Numbers + 2 Euronumbers' },
        { tierId: 8, matchText: '2 Numbers + 2 Euronumbers' },
        { tierId: 9, matchText: '3 Numbers + 1 Euronumbers' },
        { tierId: 10, matchText: '3 Numbers + 0 Euronumbers' },
        { tierId: 11, matchText: '1 Numbers + 2 Euronumbers' },
        { tierId: 12, matchText: '2 Numbers + 1 Euronumbers' }
    ];

    // Function to retrieve match text by tier id
    const getMatchTierText: (i: number) => string = i => {
        const matchArr: ITierList | undefined = tierList.find(
            id => i === id.tierId
        );
        return matchArr ? matchArr.matchText : '';
    };

    // Convert Integer to Roman Number
    // Receives a n = number and retrieves the correspondent Roman in string format.
    // https://stackoverflow.com/questions/40245626/convert-integer-to-roman-numerals-there-must-be-a-better-way
    const formatNumberToRoman: (n: number) => string = n => {
        let numerals = ['I', 'V', 'X', 'L', 'C', 'D', 'M'];
        return n
            .toString()
            .split('')
            .reverse()
            .reduce(
                (p: string, c: any, i: number) =>
                    (c === '0'
                        ? ''
                        : c < '4'
                        ? numerals[2 * i].repeat(c)
                        : c === '4'
                        ? numerals[2 * i] + numerals[2 * i + 1]
                        : c < '9'
                        ? numerals[2 * i + 1] + numerals[2 * i].repeat(c - 5)
                        : numerals[2 * i] + numerals[2 * i + 2]) + p,
                ''
            );
    };
    return { formatNumberToRoman, getMatchTierText };
};

export default Utils;
