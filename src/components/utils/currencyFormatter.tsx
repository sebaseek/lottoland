export interface ICurrencyFormatter {
    formatCurrency(locale: CurrencyLocale, currency: Currency): any;
}
export const CurrencyFormatter: () => ICurrencyFormatter = () => {
    const formatCurrency: (
        locale: CurrencyLocale,
        currency: Currency
    ) => any = (locale, currency) => {
        return new Intl.NumberFormat(locale, {
            style: 'currency',
            currency: currency
        });
    };
    return { formatCurrency };
};

export default CurrencyFormatter;

// Enum for different Currencies
export enum Currency {
    USD = 'USD',
    EUR = 'EUR'
}

// Enum for different locales.
export enum CurrencyLocale {
    EN_ES = 'en-ES',
    EN_US = 'en-US'
}
